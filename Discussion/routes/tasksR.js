// Dependencies and Modules
const express = require('express');
const cont = require('../controllers/tasksC');

// Routing Components
const route = express.Router();

// Task Routes
route.post('/', (req, res) => {
	let taskInfo = req.body;
	cont.createTask(taskInfo).then(result => res.send(result))
});

route.get('/', (req, res) => {
	cont.getAllTasks().then(result => {
		res.send(result);
	})
});

route.get('/:id', (req, res) => {
	let taskId = req.params.id;
	cont.getTask(taskId).then(outcome => res.send(outcome));
});

route.delete('/:id', (req, res) => {
	let taskId = req.params.id;
	cont.deleteTask(taskId).then(outcome => res.send(outcome));
}); 

route.put('/:id', (req, res) => {
	let taskId = req.params.id;
	let tBody = req.body
	cont.updateTask(taskId, tBody).then(outcome => {
		res.send(outcome);
	});
});

// Exports
module.exports = route;