// Dependencies and Modules
const exp = require('express');
const cont = require('../controllers/usersC');

// Routing Component
const route = exp.Router();

// User Routes
route.post('/register', (req, res) => {
	let data = req.body;
	cont.registerUser(data).then(result => {
		res.send(result)
	});
});

route.get('/', (req, res) => {
	cont.getAllUsers().then(result => res.send(result));
});

route.get('/:id', (req, res) => {
	let userId = req.params.id;
	cont.getProfile(userId).then(outcome => res.send(outcome));
});

route.delete('/:id', (req, res) => {
	let userId = req.params.id;
	cont.deleteUser(userId).then(outcome => res.send(outcome));
}); 

route.put('/:id', (req, res) => {
	let id = req.params.id;
	let katawan = req.body
	cont.updateUser(id, katawan).then(outcome => {
		res.send(outcome);
	});
});

// Exports
module.exports = route;
