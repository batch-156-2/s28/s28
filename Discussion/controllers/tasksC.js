// Dependencies and Modules
const Task = require('../models/Task.js');
const bcrypt = require('bcrypt');

// Functionalities
module.exports.createTask = (clientInput) => {
	let taskName = clientInput.name;
	let newTask = new Task({
		name: taskName
	})
	return newTask.save().then((task, error) => {
		if (error) {
			return 'Saving failed.';
		} else {
			return 'Task saved succesfully!';
		}
	});
}

module.exports.getAllTasks = () => {
	return Task.find({}).then(searchResult => {
		return searchResult;
	})
}

module.exports.getTask = (data) => {
	return Task.findById(data).then(result => {
		return result;
	});
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removed, error) => {
		if (removed) {
			return 'Task successfully removed!';
		} else {
			return 'Error. Failed to remove task!'
		}
	});
}

module.exports.updateTask = (taskId, newContent) => {
	let stat = newContent.status;
	return Task.findById(taskId).then((foundTask, err) => {
		if (foundTask) {
			foundTask.status = stat;
			return foundTask.save().then((updatedTask, err) => {
				if (err) {
					return false;
				} else {
					return 'Successfully updated task.';
				}
			})
		} else {
			return 'No task found.';
		}
	});
}