// Dependencies and Modules
const User = require('../models/User.js');
const bcrypt = require('bcrypt');

// User Functionalities
module.exports.registerUser = (reqBody) => {
	let fName = reqBody.firstName;
	let lName = reqBody.lastName;
	let email = reqBody.email;
	let passW = reqBody.password;

	let newUser = new User({
		firstName: fName,
		lastName: lName,
		email: email,
		password: bcrypt.hashSync(passW, 10)
	});
		return newUser.save().then((user, error) => {
		if (user) {
			return 'New user has been created.'
		} else {
			return 'Failed to create user.'
		}
	});
}

module.exports.getAllUsers = () => {
	return User.find({}).then(resultOfQuery => {
		return resultOfQuery;
	});
}

module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		result.password = 'Personal content. Cannot display.';
		return result;
	});
}

module.exports.deleteUser = (userId) => {
	return User.findByIdAndRemove(userId).then((removed, error) => {
		if (removed) {
			return 'User successfully removed!';
		} else {
			return 'Error. Failed to remove user!'
		}
	});
}

module.exports.updateUser = (userId, newContent) => {
	let fN = newContent.firstName;
	let lN = newContent.lastName;
	return User.findById(userId).then((foundUser, err) => {
		if (foundUser) {
			foundUser.firstName = fN;
			foundUser.lastName = lN;
			return foundUser.save().then((updatedUser, err) => {
				if (err) {
					return false;
				} else {
					return 'Successfully updated user.';
				}
			})
		} else {
			return 'No user found.';
		}
	});
}
