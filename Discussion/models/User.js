// Dependencies and Modules
const mongoose = require('mongoose');

// Schema
const userBlueprint = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First name is required.']
	},
	lastName: {
		type: String,
		required: [true, 'Last name is required.']
	},
	email: {
		type: String,
		required: [true, 'Email is required.']
	},
	password: {
		type: String,
		required: [true, 'Password is required.']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	tasks: [
		{
			taskId: {
				type: String,
				required: [true, 'Task ID is required.']
			},
			assignedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

// Exports
module.exports = mongoose.model("User", userBlueprint)
