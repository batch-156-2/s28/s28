// Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const tasksRoute = require('./routes/tasksR');
	const usersRoute = require('./routes/usersR');

// Environment Variables
	dotenv.config();
	let secret = process.env.CONNECTION_STRING;

// Server Setup
	const server = express();
	const port = process.env.PORT;

// Middelware
server.use(express.json());

// Database Connect
	mongoose.connect(secret);
	let db = mongoose.connection;
		db.once('open', () => console.log('Connection to mongoDB established.'));

// Routes
	server.use('/tasks', tasksRoute);
	server.use('/users', usersRoute);

// Server Response
	server.listen(port, () => {
		console.log(`Server is running on port: ${port}`);
	});
	server.get('/', (req, res) => {
		res.send('Welcome to my app!');
	});
	